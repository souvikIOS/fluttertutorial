import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final int days = 40;
    return Scaffold(
      appBar: AppBar(
        title: Text("Welcome to Home screen"),
      ),
      body: Center(
          child: Container(
        child: Text("Welcome to $days flutter project"),
      )),
      drawer: Drawer(),
    );
  }
}
